import React from 'react'
import './Todo.css'

export const Todo = (props) => {
    return(
      <div className="todo" >
        {props.todo.completed && <p className="todo-done">DONE</p>}
        <h4 className="name">{props.todo.name}</h4>
        <p className="dueDate">Due Date: {props.todo.dueDate}</p> 
        <p className="description">{props.todo.description}</p>
         

      </div>)
  }

