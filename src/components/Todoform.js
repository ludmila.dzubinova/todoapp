import React, { useEffect, useState } from "react";
import "./ToDoForm.css";

export const TodoForm = (props) => {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [dueDate, setDueDate] = useState("");

  useEffect(() => {
    if (props.todo) {
      setName(props.todo.name);
      setDescription(props.todo.description);
      setDueDate(props.todo.dueDate);
    }
  }, [props.todo]);

  const onFormSubmit = (e) => {
    e.preventDefault();
    if (props.todo === undefined) {
      props.onAdd({ name, description, dueDate });
    } else {
      props.onUpdate({ id: props.todo.id, name, description, dueDate });
    }
    setName("");
    setDescription("");
    setDueDate("");
  };

  return (
    <form className="form" onSubmit={onFormSubmit}>
      <h4>{props.todo ? "Edit" : "Add"} ToDo</h4>
      <div className="todo-input">
        <input
          className="todo-name"
          type="text"
          value={name}
          placeholder="Task"
          onChange={(e) => setName(e.target.value)}
        />
        <textarea
          className="todo-description"
          value={description}
          placeholder="Description..."
          onChange={(e) => setDescription(e.target.value)}
        >
          {" "}
        </textarea>
        <input
          className="todo-dueDate"
          type="date"
          value={dueDate}
          onChange={(e) => setDueDate(e.target.value)}
        />
        <button className="button add-btn" disabled={!name}>
          {props.todo ? "Edit" : "Add"} TODO
        </button>
      </div>
    </form>
  );
};
