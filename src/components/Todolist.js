import React from "react";
import { Todo } from "./Todo";
import "./Todolist.css";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import EditRoundedIcon from '@material-ui/icons/EditRounded';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

import { Flipper, Flipped } from 'react-flip-toolkit'

let ID = 1;
const getId = () => ID++;

export const TodoList = (props) => {
  const todos = props.todos;

  const [openDialog, setOpenDialog] = React.useState(false);

  const handleClickOpenDialog = () => {
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  const handleDelete = () => {
    props.onRemoveAll();
    handleCloseDialog();
  };

  if (todos.length <= 0) {
    return (
      <div className="todo-list">
        <div className="todo-header">
          <p>Nothing to do</p>
        </div>
      </div>
    );
  } else {
    return (
      <div className="todo-list">
        <div className="todo-header">
          <p>Here is your TODO list:</p>
          <p>{todos.length}</p>
          <button
            className="button remove-all-button"
            onClick={handleClickOpenDialog}
          >
            Remove all
          </button>
        </div>
        <Flipper flipKey={todos}>
        {todos.map((todo) => {
          return (
            <Flipped key={todo.id} flipId={todo.id}>
            <div className="todo-row" key={getId()}>            
              {!todo.completed && (
                <Tooltip title="Done" > 
                <IconButton aria-label="Done"  onClick={() => props.onComplete(todo.id)}>
                  <CheckBoxOutlineBlankIcon />
                </IconButton>
              </Tooltip>
              )}
              {todo.completed && (
                <Tooltip title="Undone" > 
                  <IconButton aria-label="Undone" onClick={() => props.onUnComplete(todo.id)}>
                    <CheckBoxIcon />
                  </IconButton>
                </Tooltip>
              )}
              <Todo todo={todo} />
              <Tooltip title="Edit" > 
                <IconButton aria-label="Edit" onClick={() => props.onEdit(todo.id)}>
                  <EditRoundedIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Delete" > 
                <IconButton aria-label="delete" onClick={() => props.onRemove(todo.id)} >
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            </div>
            </Flipped>
          );
        })}
        </Flipper>
        <Dialog
          open={openDialog}
          onClose={handleCloseDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            Are you sure you want to delete all ToDo items?
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Deleting all todo items cannot be reverted.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseDialog} color="primary">
              Cancel
            </Button>
            <Button onClick={handleDelete} color="primary" autoFocus>
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
  }