import React, { useState, useEffect, useCallback } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import axios from "axios";

import { Header } from "./components/Header";
import { TodoList } from "./components/Todolist";
import { TodoForm } from "./components/Todoform";

const App = () => {
  const [todos, setTodos] = useState([]);
  const [editingId, setEditingId] = useState("");

  const loadTodos = useCallback(async () => {
    const response = await axios.get("http://localhost:3001/todos");
    setTodos(response.data);
  }, []);

  useEffect(() => {
    console.log("hello from useEffect");
    loadTodos();
  }, [loadTodos]);

  const addTodo = async (todo) => {
    // setTodos([...todos, todo])
    const newTodo = {
      name: todo.name,
      description: todo.description,
      dueDate: todo.dueDate,
      completed: false,
    };
    await axios.post("http://localhost:3001/todos", newTodo);
    loadTodos();
  };

  const editTodo = async (todo) => {
    await axios.put(`http://localhost:3001/todos/${todo.id}`, todo);
    setEditingId("");
    loadTodos();
  };

  const startEditing = (id) => {
    setEditingId(id);
  };

  const removeTodo = async (id) => {
    await axios.delete("http://localhost:3001/todos/" + id);
    loadTodos();
  };

  const removeAll = async () => {
    await axios.delete("http://localhost:3001/todos/delete/all");
    loadTodos();

    // setTodos([])
  };

  const completeTodo = async (id) => {
    await axios.put("http://localhost:3001/todos/" + id, { completed: true });
    loadTodos();
  };

  const unCompleteTodo = async (id) => {
    await axios.put("http://localhost:3001/todos/" + id, { completed: false });
    loadTodos();
  };

  const getEditingTodo = () => {
    const todo = todos.find((todo) => todo.id === editingId);
    return todo;
  };

  return (
    <div className="app">
      <div className="container">
        <Header
          title="TODO LIST"
          isVisible={true}
          subtitle="Your busy life deserves this!"
        ></Header>
        <div>
          <TodoForm
            todo={getEditingTodo()}
            onAdd={addTodo}
            onUpdate={editTodo}
          ></TodoForm>
          <TodoList
            todos={todos}
            onRemove={removeTodo}
            onComplete={completeTodo}
            onUnComplete={unCompleteTodo}
            onRemoveAll={removeAll}
            onEdit={startEditing}
          />
        </div>
      </div>
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
